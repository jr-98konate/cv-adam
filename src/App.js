import React from 'react';
import {MdWork} from "react-icons/md"
import './index.css';
import {HiOutlineAcademicCap} from "react-icons/hi"
import {GiHobbitDoor} from "react-icons/gi"
import {AiOutlinePhone} from "react-icons/ai"
import {BiMap} from "react-icons/bi"
import {CiMail} from "react-icons/ci"
import profil from "./src/profil.jpeg"

function App() {
  
    return (
      <div className="container-fluid">
       
        <div className='row pricipale '>
          <div className=' col-lg-2 col-md-3 col-sm-12   left'>
            <div className="text-center m-3">
              <img className="rounded-circle " src={profil} alt="" />
            </div>
            <h3>Adama soumaré</h3>
            <div className="num d-flex mt-3 ">
              <span className="icons-l mt-3"><AiOutlinePhone/></span>
              <p className="mx-3">77 961 91 36 <br/> 76 396 97 93</p>
            </div>
             <div className="num d-flex  ">
              <span className="icons-l"><CiMail/></span>
              <p className="mx-3">drysou496@gmail.com</p>
            </div>
            <div className="num d-flex  ">
              <span className="icons-l"><BiMap/></span>
              <p className="mx-3">Rue 09x08 Medina Dakar</p>
            </div>
            {/* compétence */}

            
            <div className="compte mx-3 mt-5">
            <h3 className="comp">Compétences:</h3>
                <p className="">Adobe Photoshop</p>
                <p className="">Adobe illustrator</p>
                <p className="">Adobe InDesign</p>
                <p className="">Adobe Premier Pro</p>
                <p className="">Adobe Audition</p>
                <p className="">Adobe After effects</p>
                <p className="">Worpress</p>
                <p className="">Final Cut</p>
              
            </div>
            
            <div className="compte mx-3 mt-5">
            <h3 className="comp">Langues:</h3>
                <p className="">Français: Courant</p>
                <p className="">Arabe: Courant</p>
                <p className="">Anglais: Intermmédaire</p>
                <p className="">Wolof: Courant</p>
                <p className="">Soninké: Courant</p>
              
            </div>

          </div>
          <div className=' col-md-5 col-sm-12 m-3'>
            {/* commentaire */}
            {/* <div className='row '>
              <div className='card border-0 shadow'>
                <div className='d-flex'>
                  <span>icon</span>
                  <h4 className='mx-3'>A Propos de moi</h4>
                </div>
                <p className='mx-5'>Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.</p>
              </div>
            </div>  */}
            <div className='row mt-2'>
              <div className='card border-0  shadow'>
                <div className='d-flex mt-3 m-3'>
                  <span><MdWork/></span>
                  <h4 className='mx-3 '>Expériences:</h4>
                  
                </div>
                
                <h5 className='mx-5 text-bold'>Association (Medina en couleurs)</h5>
                <span className='mx-5 text-primary'>Depuis 2018  </span>
                <p className='mx-5'>Don de sang, conférences religieuses, don de nourriture, consultations suivies de don de lunnettes, organisation de génie en herbes, séminaires de formation, ect.</p>
                <hr/>
                <div className="mb-3">
                <h5 className='mx-5'> Equipe de football entraîneur</h5>
                <span className='mx-5 text-primary'>2018  - 2019</span>
               
                <p className='mx-5'>Être à la tête de deux équipes scolaires, celle de mon école ainsi que celle de ma classe m'as permis d'accoître mes compétences (prise de décision, esprit d'équipe, ect.)</p>

                </div>
                
              </div>
            </div>
             {/* formation */}
            <div className='row mt-2'>
              <div className='card border-0 shadow'>
                <div className='d-flex m-3'>
                  <span><HiOutlineAcademicCap/></span>
                  <h4 className='mx-3 '>FORMATION:</h4>
                  
                </div>
                <div className="formation mx-5 ">
                  
                    <h6 className=''>  BTS Production Audiovisuel <br/><span className='text-primary mr-3'>2022</span></h6>
                    <hr/>
                    <h6 className=''>  Baccalauréat Êtat (passable) <br/><span className='text-primary mr-3'>2019</span></h6>
                    <hr/>
                    <h6 className=''>  Baccalauréat Arabe (Bien) <br/><span className='text-primary mr-3'>2019</span></h6>
                    <hr/>
                    <h6 className=''>  Brevet : (Bien) <br/><span className='text-primary mr-3'>2016</span></h6>
                    <hr/>
                    <h6 className=''> CFEE:  (Excellent) <br/><span className='text-primary mr-3'>2012</span></h6>
                    <hr/>
                    
                  
                  
                </div>
                
                
              </div>
            </div>
              {/* hobby */}
              <div className="row mt-2">
              <div className='card border-0 shadow'>
                <div className='d-flex m-3'>
                  <span><GiHobbitDoor/></span>
                  <h4 className='mx-3'>Centre d'intérêts:</h4>
                </div>
                <div className='hobby  d-flex  '>
                  <p className='mx-3'>Lecture</p> 
                  <p className='mx-3'>Musique</p>
                  <p className='mx-3'>Sport</p>
                </div>
              </div>
              </div>
              
          </div>
        </div>
      </div>
    );
  
  
}

export default App;
